<?php

use Illuminate\Database\Seeder;

class PostingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('postings')->insert([
            'name' => 'Tereza Simcic',
            'city' => 'Gonjace',
            'country' => 'Slovenia',
            'phone' => '123456',
            'address' => 'address1',
        ]);
        DB::table('postings')->insert([
            'name' => 'Joe Doe',
            'city' => 'New York City',
            'country' => 'USA',
            'phone' => '123457',
            'address' => 'address2',
        ]);
        DB::table('postings')->insert([
            'name' => 'Mary Smith',
            'city' => 'Milton Keynes',
            'country' => 'UK',
            'phone' => '123458',
            'address' => 'address3',
        ]);
        DB::table('postings')->insert([
            'name' => 'Maria Giovanneti',
            'city' => 'Roma',
            'country' => 'Italia',
            'phone' => '123459',
            'address' => 'address4',
        ]);

    }
}
