<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Postings extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'city', 'country','phone','address',
    ];
}
