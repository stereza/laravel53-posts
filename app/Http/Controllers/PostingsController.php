<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Postings;
use DB;

class PostingsController extends Controller
{
    protected $rules;

    public function __construct()
    {
        $this->rules = [
            'name' => 'required',
            'city' => 'required|min3',
            'coutry' => 'required|min2',
        ];
    }
    public function index()
    {
        // $posts = DB::table('postings')->get();
        $posts = Postings::all();
        // $posts = Postings::select('name', 'city', 'country')->get();
        // dd($posts);
        return view('postings.index', ['posts'=> $posts]);
    }

    public function show(Postings $posting)
    {
        // $post = Postings::select('name', 'city', 'country')->where('id', $posting->id)->first();
        $post = Postings::findOrFail($posting->id, ['name','city','country']);
        return view('postings.show',  ['post' => $post]);
    }
}
