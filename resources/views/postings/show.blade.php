@extends ('layouts.master')

@section('content')

    <div class="col-md-6 col-md-offset-3">

     @if (!empty($post))
        <h1>Post's name:  {{ $post->name}} <small><a href="/postings/{{$post->id}}/edit">Edit</a></small> </h1>
        <dl class="dl-horizontal">
            <dt>Post's Name</dt>
            <dd>{{ $post->name }}</dd>
            <dt>Post's city</dt>
            <dd>{{ $post->city }}</dd>
            <dt>Post's country</dt>
            <dd>{{ $post->country }}</dd>
        </dl>

    @endif

    <p class="pull-left"><a href="/"> Back </a></p>
    </div>
@stop