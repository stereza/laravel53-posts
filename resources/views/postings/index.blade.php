@extends ('layouts.master')

@section('content')
    <h1>Posts</h1>
    @if (!empty($posts))
        @foreach ($posts as $post)
            <li> <a href="/postings/{{ $post->id }}"> {{ $post->name }} </a></li>

        @endforeach
    @endif
@stop
